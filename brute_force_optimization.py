from itertools import combinations

import numpy as np
import torch

import utils.boxmap_represent as boxmap_represent
import utils.utils as utils
from utils.utils import calc_iou, boxExpansion


def initPrune_and_Cluster(boxmap,boxes,boxmapDetThreshold,boxPruneThreshold,clusterThreshold):
	# Prune boxes outside the boxmap object area
	clone_boxmap = np.copy(boxmap)
	clone_boxmap[clone_boxmap>boxmapDetThreshold]=1.0
	prune_boxes = []
	for inds,a_box in enumerate(boxes):
		box_region = clone_boxmap[int(a_box[1]):int(a_box[3]),int(a_box[0]):int(a_box[2])]
		region_area = (box_region==1.0).sum()
		if (region_area) >= boxPruneThreshold * (box_region.shape[0]*box_region.shape[1]):
			prune_boxes.append(a_box)
	prune_boxes = np.stack(prune_boxes)
	prune_boxes = prune_boxes.tolist()
	# Make clusters of boxes when boxes overlaps
	clusters = []
	while prune_boxes!=[]:
		box1 = prune_boxes[0]
		a_cluster = []
		a_cluster.append(box1)
		expanded_box = box1
		for inds2,box2 in enumerate(prune_boxes):
			if inds2!=0:
				is_overlap = calc_iou(np.array(expanded_box),np.array(box2))
				if is_overlap>=clusterThreshold:
					a_cluster.append(box2)
					expanded_box = boxExpansion(expanded_box,box2)
		prune_boxes = [x for x in prune_boxes if x not in a_cluster]
		clusters.append(a_cluster)
	return clusters

def optimize(boxes,boxmap,boxmapDetThreshold=0.5,boxPruneThreshold=0.5,clusterThreshold=0.10,maxOverlap=6):
	"""
	Parameters
	----------
	boxes : [N,4] numpy array boxes
	boxmap: 2-d numpy array boxmap where each pixel is represented by the number of overlapped object
	boxmapDetThreshold: clamped pixel value threshold for boxmap 
	boxPruneThreshold: threshold to prune boxes outside of the boxmap area
	clusterThreshold: Boxes overlap threshold for clustering
	maxOverlap: Maximum value in boxmap. By default we set it to 6. Because, there are no pixel in our current dataset which overlap more than 6 objects.
				One can change it to number of boxes N.
	Returns
	-------
	Final_boxes : Optimized [N,4] numpy array boxes 
	"""
	clusters = initPrune_and_Cluster(boxmap,boxes,boxmapDetThreshold,boxPruneThreshold,clusterThreshold)
	# Optimize each cluster seperately
	final_boxes = []
	for a_cluster in clusters:
		a_cluster = set(tuple(row) for row in a_cluster)
		optimum_dis=99999999999
		optimum_cluster = None
		if len(a_cluster)<maxOverlap-1: 
			max_com = len(a_cluster)+1
		else:
			max_com=maxOverlap 
		for i in range(1,max_com):
			cluster_combs = list(set(combinations(a_cluster, i)))
			for a_cluster_comb in cluster_combs:
				a_cluster_comb = list(a_cluster_comb)
				pred_img = boxmap_represent.boxmapCreate(a_cluster_comb,boxmap.shape)
				distance = np.linalg.norm(boxmap-pred_img)
				if distance<optimum_dis:
					optimum_dis = distance
					optimum_cluster = a_cluster_comb
				pred_img[:,:]=0
		optimum_cluster_boxes = [list(a_box) for a_box in optimum_cluster]
		final_boxes = final_boxes + optimum_cluster_boxes
	return np.asarray(final_boxes)

def brute_force_optimize(boxes, boxmap):
	return optimize(boxes[:,0:4],boxmap,0.5,0.5,.10,6)
