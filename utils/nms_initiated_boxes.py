import numpy as np
import itertools
import functools

from nms import nms_optimize
from utils.utils import eval_fnc

def powerset(items, depth=None):
	"""Return all subsets of `items`.

	Parameters
	----------
	items : list
		The universal set.
	depth : int
		If specified, the maximum length of returned subsets is limited to the value of `depth`.
	"""
	if depth is None:
		depth = len(items)
	for m in range(depth + 1):
		for b in itertools.combinations(items, m):
			yield b
def with_nms_initialization(func):
	@functools.wraps(func)
	def wrapper(boxes,given_box_map):
		nms_boxes = nms_optimize(boxes,given_box_map)
		nms_boxes = [*nms_boxes]
		best_cost = np.inf
		# Find out best combination of boxmap boxes from nms boxes 
		for comb_idxs in powerset(list(range(len(nms_boxes)))):
			comb = [nms_boxes[c] for c in comb_idxs]
			cost,out_boxmap = eval_fnc(comb, given_box_map,None)
			if cost <= best_cost:
				best_cost = cost
				best_box_idxs = comb_idxs
				best_boxes = comb
				best_boxmap = out_boxmap
		remaining_boxes = np.array(list(set(map(tuple,boxes)) - set(map(tuple,best_boxes))))
		remaining_boxes = remaining_boxes.astype(int)
		best_indices = [np.where((boxes==a_comb))[0][0] for a_comb in comb]
		final_boxes = func(boxes,given_box_map,best_cost,best_indices,best_boxmap,remaining_boxes)
		return final_boxes
	return wrapper