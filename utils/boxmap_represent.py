from itertools import combinations

import numpy as np


def doOverlap(A, B):
	"""
	Check whether two boxes A and B overlaps
	"""
	if A[0] > B[2] or B[0] > A[2]: 
		return False
	if A[1] > B[3] or B[1] > A[3]:
		return False
	return True

def overlapCheck(boxes):
	"""
	Check whether a certain region overlapped by all boxes
	"""
	pair_combs = list(combinations(boxes,2))
	for a_pair in pair_combs:
		overlap = doOverlap(a_pair[0],a_pair[1])
		if overlap==False:
			return False
	return True

def boxmapCreate(boxes, imageShape, image=None):
	"""Render a boxmap image.

	Parameters
	----------
	boxes : np.ndarray
		[N,4] numpy array boxes
	imageShape : tuple
		(height, width) represent image height and width
	image : :obj:`np.ndarray`, optional
		Initial image to render the boxes on. If not specified, the boxes will be rendered over a blank image.
	Returns
	-------
	np.ndarray : 2-dimensional boxmap image which shape equal to imageShape
	"""
	if image is None:
		image = np.zeros((imageShape))
	for a_box in np.asarray(boxes, dtype=np.int32):
		image[a_box[1]:a_box[3], a_box[0]:a_box[2]] += 1
	return image
