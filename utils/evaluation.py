from dataclasses import dataclass
from typing import List

import numpy as np

from utils.utils import calc_iou

_INF_LRP = {
	'lrp': np.inf,
	'lrp_iou': np.inf,
	'lrp_fp': np.inf,
	'lrp_fn': np.inf,
}


@dataclass
class _MatchBoxResults:
	"""Represents pairs of matching boxes.

	Given a list of predicted boxes and a list of ground truth boxes, an object of this class stores:
	* Parallel lists containing the indices of matching boxes and the IoU between them.
	* Confidence scores for the whole list of predicted boxes.
	* The length of the list of ground truth boxes.
	"""

	pred_indices: np.ndarray  # Indices of matching predicted boxes.
	gt_indices: np.ndarray  # Indices of matching ground truth boxes.
	ious: np.ndarray  # IoUs between matching pairs of boxes.
	all_confidence_scores: np.ndarray  # Confidence scores for ALL (not just matching) predicted boxes.
	n_gt_boxes: int  # Number of ALL (not just matching) ground truth boxes.

	@property
	def n_pred_boxes(self):
		return len(self.all_confidence_scores)

	def filter_by_confidence(self, confidence_threshold):
		all_mask = self.all_confidence_scores > confidence_threshold
		match_mask = all_mask[self.pred_indices]
		return _MatchBoxResults(
			pred_indices=self.pred_indices[match_mask],
			gt_indices=self.gt_indices[match_mask],
			ious=self.ious[match_mask],
			all_confidence_scores=self.all_confidence_scores[all_mask],
			n_gt_boxes=self.n_gt_boxes,
		)

	def filter_by_ground_truth(self, keep_gt_indices):
		"""Filter match box results by ground truth box indices to keep.

		Note that this is different to simply omitting these ground truth boxes in the first place, since predicted
		boxes which match one of these boxes should not be counted as false positives (following "Pedestrian
		Detection: An Evaluation of the State of the Art").
		"""
		match_mask = np.in1d(self.gt_indices, keep_gt_indices)
		rm_pred_indices = self.pred_indices[np.logical_not(match_mask)]
		return _MatchBoxResults(
			pred_indices=self.pred_indices[match_mask],
			gt_indices=self.gt_indices[match_mask],
			ious=self.ious[match_mask],
			all_confidence_scores=np.delete(self.all_confidence_scores, rm_pred_indices),
			n_gt_boxes=len(keep_gt_indices),
		)

	def true_positive_mask(self):
		tp = np.zeros(self.n_pred_boxes)
		tp[self.pred_indices] = 1
		return tp


def match_boxes(pred_boxes: np.ndarray, gt_boxes: np.ndarray, ignored_gt_indices=None, iou_threshold=0.5, policy='coco', is_pixel_box=True):
	"""Match predicted boxes with ground truth boxes based on IoU.

	For most applications you will want to sort `pred_boxes` in descending order of confidence first.

	Parameters
	----------
	pred_boxes : (M, 4) ndarray
	gt_boxes : (N, 4) ndarray
	iou_threshold : float
	ignored_gt_indices : (N,) ndarray
	policy : str
	is_pixel_box : bool

	Returns
	-------
	match_pred_indices : (K,) ndarray
	match_gt_indices : (K,) ndarray
	match_ious : (K,) ndarray
	"""
	assert pred_boxes.shape[-1] == 4
	assert gt_boxes.shape[-1] == 4
	if policy == 'coco':
		n_check = len(gt_boxes)
	elif policy == 'pascal_voc':
		n_check = 1
	else:
		raise ValueError('unrecognised matching policy')
	iou_matrix = calc_iou(pred_boxes, gt_boxes, is_pixel_box=is_pixel_box)
	already_matched = set()
	if ignored_gt_indices is None:
		ignored = set()
	else:
		ignored = set(ignored_gt_indices)
	match_pred_indices = []
	match_gt_indices = []
	match_ious = []
	for i in range(len(pred_boxes)):
		ious = iou_matrix[i]
		gt_box_order = np.argsort(ious, kind='stable')[::-1]
		matched = False
		for j in gt_box_order[:n_check]:
			# Skip ignored boxes.
			if j in ignored:
				continue
			iou = ious[j]
			if iou > iou_threshold:
				if j not in already_matched:
					# This predicted box is a true positive.
					already_matched.add(j)
					match_pred_indices.append(i)
					match_gt_indices.append(j)
					match_ious.append(iou)
					matched = True
					break
			else:
				# This predicted box is either a false positive or matches an ignored box.
				break
		# Consider ignored ground truth boxes. We adhere to the following rules from "Pedestrian Detection: An
		# Evaluation of the State of the Art", section 3.2:
		# 1. Use IoU threshold of 0 for ignored GT boxes ("can match any subregion of a BBig").
		# 2. Prefer matching unignored GT boxes first ("can only match a BBig if it does not match any BBgt").
		# 3. Allow multiple matches with ignored GT boxes ("multiple matches to a single BBig are allowed").
		if not matched:  # Rule #2.
			for j in gt_box_order:
				iou = ious[j]
				if iou > 0:  # Rule #1.
					if j in ignored:
						# This predicted box matches an ignored ground truth box.
						# Note: We don't add j to `already_matched` because of rule #3.
						match_pred_indices.append(i)
						match_gt_indices.append(j)
						match_ious.append(iou)
						break
				else:
					# This predicted box is a false positive.
					break
	match_pred_indices = np.asarray(match_pred_indices, dtype=np.int)
	match_gt_indices = np.asarray(match_gt_indices, dtype=np.int)
	match_ious = np.asarray(match_ious)
	return match_pred_indices, match_gt_indices, match_ious


def _get_match_box_results_list(pred_boxes_list, gt_boxes_list, ignored_gt_indices_list=None, iou_threshold=0.5, policy='coco'):
	match_box_results_list = []
	for i, (pred_boxes, gt_boxes) in enumerate(zip(pred_boxes_list, gt_boxes_list)):
		# Sort predictions based on confidence.
		pred_box_order = np.argsort(pred_boxes[:, 4], kind='stable')[::-1]
		pred_boxes = pred_boxes[pred_box_order]
		# Match predicted boxes with ground truth boxes.
		if ignored_gt_indices_list is None:
			ignored_gt_indices = None
		else:
			ignored_gt_indices = ignored_gt_indices_list[i]
		match_pred_indices, match_gt_indices, match_ious = \
			match_boxes(pred_boxes[:, :4], gt_boxes, ignored_gt_indices=ignored_gt_indices, iou_threshold=iou_threshold, policy=policy)
		match_box_results = _MatchBoxResults(
			match_pred_indices,
			match_gt_indices,
			match_ious,
			pred_boxes[:, 4],
			len(gt_boxes),
		)
		if ignored_gt_indices_list is not None:
			keep_gt_indices = np.setdiff1d(np.arange(len(gt_boxes)), ignored_gt_indices_list[i])
			match_box_results = match_box_results.filter_by_ground_truth(keep_gt_indices)
		match_box_results_list.append(match_box_results)
	return match_box_results_list


def calc_precision_recall_curve(tp_mask: np.ndarray, n_condition_positive: int, force_monotonic: bool = True):
	"""Calculate the points on a precision-recall curve.

	Points will be added for recall = 0 and recall = 1. To associate the recall and precision values with the original
	indices, drop the first and last values.

	Parameters
	----------
	tp_mask : (N,) ndarray
		Boolean array of true positive flags corresponding to a list of predictions ordered from most to least
		confident.
	n_condition_positive : int
		The total number of condition positives.
	force_monotonic : bool
		If true, replace each precision value with the maximum precision value to the right of that recall level
		(inclusive).
	Returns
	-------
	recall : (N + 2,) ndarray
		Recall values.
	precision : (N + 2,) ndarray
		Precision values.
	"""
	tp = np.cumsum(tp_mask)
	fp = np.cumsum(1 - tp_mask)

	# Calculate points on the precision-recall graph.
	recall = tp / n_condition_positive
	precision = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)

	# Insert the (0, 0) and (1, 0) points.
	precision = np.concatenate(([0.], precision, [0.]))
	recall = np.concatenate(([0.], recall, [1.]))

	if force_monotonic:
		# Remove the "wiggles" in the graph.
		precision_rev = np.flip(precision, 0)
		np.maximum.accumulate(precision_rev, out=precision_rev)

	return recall, precision


def average_precision_auc(recall, precision):
	# Used in PASCAL VOC2010--2012
	ids = np.where(recall[1:] != recall[:-1])[0]
	return np.sum((recall[ids + 1] - recall[ids]) * precision[ids + 1])


def average_precision_interp(recall, precision, n_points=101):
	# Used in COCO (n_points = 101)
	# Used in PASCAL VOC2008 (n_points = 11)
	return np.interp(np.linspace(0.0, 1.0, n_points), recall, precision).mean()


def _calc_ap(match_box_results_list: List[_MatchBoxResults]):
	conf_list = np.concatenate([e.all_confidence_scores for e in match_box_results_list], axis=0)
	conf_order = np.argsort(conf_list, kind='stable')[::-1]
	tp_mask = np.concatenate([e.true_positive_mask() for e in match_box_results_list], axis=0)[conf_order]
	n_condition_positive = sum(match_box_results.n_gt_boxes for match_box_results in match_box_results_list)
	recall, precision = calc_precision_recall_curve(tp_mask, n_condition_positive, force_monotonic=True)
	return average_precision_auc(recall, precision)


def _calc_lrp(match_box_results_list: List[_MatchBoxResults], iou_threshold=0.5, confidence_threshold=None):
	total_iou = 0
	# True positive
	n_tp = 0
	# Predicted condition positive
	n_y = 0
	for match_box_results in match_box_results_list:
		if confidence_threshold is not None:
			match_box_results = match_box_results.filter_by_confidence(confidence_threshold)
		total_iou += match_box_results.ious.sum()
		n_tp += len(match_box_results.ious)
		n_y += match_box_results.n_pred_boxes

	# Condition positive
	n_x = sum(match_box_results.n_gt_boxes for match_box_results in match_box_results_list)
	# False positive
	n_fp = n_y - n_tp
	# False negative
	n_fn = n_x - n_tp

	# LRP_fp = 1 - precision
	if n_y == 0:
		lrp_fp = np.inf
	else:
		lrp_fp = n_fp / n_y

	# LRP_fn = 1 - recall
	if n_x == 0:
		lrp_fn = np.inf
	else:
		lrp_fn = n_fn / n_x

	# LRP_IoU
	if n_y == 0:
		lrp_iou = np.inf
	else:
		lrp_iou = 1 - total_iou / n_tp

	z = n_tp + n_fp + n_fn
	if z == 0:
		lrp = np.inf
	else:
		w_iou = n_tp / (1 - iou_threshold)
		w_fp = n_y
		w_fn = n_x
		lrp = (w_iou * lrp_iou + w_fp * lrp_fp + w_fn * lrp_fn) / z

	return {
		'lrp': lrp,
		'lrp_iou': lrp_iou,
		'lrp_fp': lrp_fp,
		'lrp_fn': lrp_fn,
	}


def _calc_precision(match_box_results_list: List[_MatchBoxResults]):
	# True positive
	n_tp = 0
	# Predicted condition positive
	n_y = 0
	for match_box_results in match_box_results_list:
		n_tp += len(match_box_results.ious)
		n_y += match_box_results.n_pred_boxes
	# False positive
	n_fp = n_y - n_tp
	# Precision = TP / (TP + FP)
	return n_tp / (n_tp + n_fp)


def _calc_recall(match_box_results_list: List[_MatchBoxResults]):
	# True positive
	n_tp = 0
	# Predicted condition positive
	for match_box_results in match_box_results_list:
		n_tp += len(match_box_results.ious)
	# Condition positive
	n_x = sum(match_box_results.n_gt_boxes for match_box_results in match_box_results_list)
	# False negative
	n_fn = n_x - n_tp
	# Recall = TP / (TP + FN)
	return n_tp / (n_tp + n_fn)


def calc_ap(pred_boxes_list, gt_boxes_list, ignored_gt_indices_list=None, iou_threshold=0.5, policy='coco'):
	"""Calculate the average precision across a number of images.

	This is not a simple mean across the AP for each image---it considers all of the boxes together, effectively
	weighting images with more boxes higher.

	Parameters
	----------
	pred_boxes_list : list of array_like
		Predicted boxes for each of the N images.
	gt_boxes_list : list of array_like
		Ground truth boxes for each of the N images.
	ignored_gt_indices_list: list of array_like
		Indices of ground truth boxes to ignore for each of the N images. For details on how ignored ground truth boxes
		are treated, see section 3.2 of "Pedestrian Detection: An Evaluation of the State of the Art".
	iou_threshold : float
		Intersection over union (IoU) threshold for matching boxes.
	policy : str
		Matching policy ("coco" or "pascal_voc").

	Returns
	-------
	ap : float
	"""
	match_box_results_list = _get_match_box_results_list(
		pred_boxes_list,
		gt_boxes_list,
		ignored_gt_indices_list=ignored_gt_indices_list,
		iou_threshold=iou_threshold,
		policy=policy,
	)
	return _calc_ap(match_box_results_list)


def calc_lrp(pred_boxes_list, gt_boxes_list, ignored_gt_indices_list=None, iou_threshold=0.5, confidence_threshold=None):
	match_box_results_list = _get_match_box_results_list(
		pred_boxes_list,
		gt_boxes_list,
		ignored_gt_indices_list=ignored_gt_indices_list,
		iou_threshold=iou_threshold,
		policy='coco',
	)
	return _calc_lrp(match_box_results_list, iou_threshold, confidence_threshold)


def calc_optimal_lrp(pred_boxes_list, gt_boxes_list, ignored_gt_indices_list=None, iou_threshold=0.5, n_points=101):
	match_box_results_list = _get_match_box_results_list(
		pred_boxes_list,
		gt_boxes_list,
		ignored_gt_indices_list=ignored_gt_indices_list,
		iou_threshold=iou_threshold,
		policy='coco',
	)
	best_lrp = _INF_LRP
	best_s = 0
	for s in np.linspace(0.0, 1.0, n_points):
		lrp = _calc_lrp(match_box_results_list, iou_threshold, s)
		if lrp['lrp'] < best_lrp['lrp']:
			best_s = s
			best_lrp = lrp
	return best_lrp, best_s


def calc_precision(pred_boxes_list, gt_boxes_list, ignored_gt_indices_list=None, iou_threshold=0.5, confidence_threshold=None):
	match_box_results_list = _get_match_box_results_list(
		pred_boxes_list,
		gt_boxes_list,
		ignored_gt_indices_list=ignored_gt_indices_list,
		iou_threshold=iou_threshold,
		policy='coco',
	)
	if confidence_threshold:
		match_box_results_list = [
			mbr.filter_by_confidence(confidence_threshold)
			for mbr in match_box_results_list
		]
	return _calc_precision(match_box_results_list)


def calc_recall(pred_boxes_list, gt_boxes_list, ignored_gt_indices_list=None, iou_threshold=0.5, confidence_threshold=None):
	match_box_results_list = _get_match_box_results_list(
		pred_boxes_list,
		gt_boxes_list,
		ignored_gt_indices_list=ignored_gt_indices_list,
		iou_threshold=iou_threshold,
		policy='coco',
	)
	if confidence_threshold:
		match_box_results_list = [
			mbr.filter_by_confidence(confidence_threshold)
			for mbr in match_box_results_list
		]
	return _calc_recall(match_box_results_list)
