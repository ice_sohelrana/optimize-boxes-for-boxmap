import functools

from PIL import Image
from torchvision import transforms as T
import torch, numpy
def downsampling_cluster(boxes,boxmap,max_size=512):
	"""
	Reduce the cluster boxmap size 
	"""
	if boxmap.shape[0]>max_size or boxmap.shape[1]>max_size:
		downsampling_ratio = max(boxmap.shape[0]/max_size,boxmap.shape[1]/max_size)
		y_size = int(round(boxmap.shape[0]/downsampling_ratio))
		x_size = int(round(boxmap.shape[1]/downsampling_ratio))
		resize = T.Resize((y_size,x_size),interpolation=Image.NEAREST)
		boxmap = resize(torch.from_numpy(boxmap).unsqueeze(dim=0)).squeeze(dim=0).numpy()
	else:
		downsampling_ratio = 1
	boxes[:,0:4] = boxes[:,0:4]/downsampling_ratio
	return boxmap,boxes,downsampling_ratio	
def upsampling_cluster(boxes,sampling_ratio):
	"""
	Resize the cluster size to get the actual boxes 
	"""
	boxes[:,0:4] = boxes[:,0:4]*sampling_ratio
	return boxes

def with_max_image_size(max_size):
	def with_max_image_size_impl(func):
		if max_size <= 0:
			return func
		@functools.wraps(func)
		def wrapper(boxes, boxmap):
			smaller_boxmap, smaller_boxes, ratio = downsampling_cluster(boxes, boxmap, max_size)
			final_boxes = func(smaller_boxes, smaller_boxmap)
			return upsampling_cluster(final_boxes, ratio)
		return wrapper
	return with_max_image_size_impl
