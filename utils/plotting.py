import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from matplotlib.patches import Rectangle


def plot_boxmap(boxmap, boxes=None):
	im = Image.fromarray(np.array(boxmap, dtype=np.uint8, copy=True))
	fig, ax = plt.subplots(1, 1)
	im_plot = ax.imshow(im, cmap="magma")
	if boxes is not None:
		for x1, y1, x2, y2 in boxes[..., :4]:
			rect = Rectangle((x1, y1), x2 - x1, y2 - y1, linewidth=1, edgecolor='#00ff00',
							 facecolor='none')
			ax.add_patch(rect)
	fig.colorbar(im_plot)
	fig.tight_layout()
	plt.close(fig)
	return fig
