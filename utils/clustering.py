import functools
import matplotlib.pyplot as plt

# from utils.sub_clustering import sub_clustering
# from utils.cluster_resize import downsampling_cluster,upsampling_cluster

import numpy as np
from aabbtree import AABB, AABBTree
import nms

_null_aabb = AABB()


def get_tree_items(tree):
	"""Get all entries in the tree."""
	items = []
	if tree.aabb != _null_aabb and tree.value is not None:
		items.append((tree.aabb, tree.value))
	if tree.left is not None:
		items.extend(get_tree_items(tree.left))
	if tree.right is not None:
		items.extend(get_tree_items(tree.right))
	return items


def extend_tree_(tree, items):
	"""Extend a tree with more entries."""
	for aabb, value in items:
		tree.add(aabb, value)


def are_trees_overlapping(tree1, tree2):
	"""Check whether two AABB trees overlap.

	Parameters
	----------
	tree1 : AABBTree
	tree2 : AABBTree

	Returns
	-------
	bool
		True if the trees overlap, False otherwise.
	"""
	if not tree2.does_overlap(tree1.aabb):
		return False
	for aabb, _ in get_tree_items(tree1):
		if tree2.does_overlap(aabb):
			return True
	return False


def cluster_boxes(boxes):
	"""Cluster overlapping groups of boxes together.

	Parameters
	----------
	boxes : np.ndarray
		[N,5] numpy array boxes with confidence scores

	Returns
	-------
	clustered_boxes : list
	cluster_limits : list
	"""
	aabbs = []
	for x1, y1, x2, y2, confidence in boxes:
		aabbs.append(AABB([(x1, x2), (y1, y2)]))
	raw_clusters = []
	for i, aabb in enumerate(aabbs):
		cluster = AABBTree()
		cluster.add(aabb, i)
		raw_clusters.append(cluster)
	good_clusters = []
	to_ignore = set()
	for i, cluster in enumerate(raw_clusters):
		if i in to_ignore:
			continue
		cluster_changed = True
		while cluster_changed:
			cluster_changed = False
			for j, other_cluster in enumerate(raw_clusters):
				if i == j or j in to_ignore:
					continue
				if are_trees_overlapping(cluster, other_cluster):
					extend_tree_(cluster, get_tree_items(other_cluster))
					cluster_changed = True
					to_ignore.add(j)
		good_clusters.append(cluster)
	clustered_boxes = []
	cluster_limits = []
	for cluster in good_clusters:
		indices = [value for _, value in get_tree_items(cluster)]
		clustered_boxes.append(boxes[indices].copy())
		[(x1, x2), (y1, y2)] = cluster.aabb.limits
		x1 = int(np.floor(x1))
		y1 = int(np.floor(y1))
		x2 = int(np.ceil(x2)) + 1
		y2 = int(np.ceil(y2)) + 1
		cluster_limits.append([(x1, x2), (y1, y2)])
	return clustered_boxes, cluster_limits


def split_boxmap(boxmap, clustered_boxes, cluster_limits):
	"""Split a boxmap into subimages based on box clustering.

	Parameters
	----------
	boxmap : np.ndarray
	clustered_boxes : list
	cluster_limits : list

	Returns
	-------
	subboxes : list
	subboxmaps : list
	"""
	subboxes = []
	subboxmaps = []
	for b, limits in zip(clustered_boxes, cluster_limits):
		[(x1, x2), (y1, y2)] = limits
		b[..., [0, 2]] -= x1
		b[..., [1, 3]] -= y1
		subboxes.append(b)
		subboxmaps.append(boxmap[y1:y2, x1:x2])
	return subboxes, subboxmaps


def with_clustering(func):
	@functools.wraps(func)
	def wrapper(boxes, boxmap):
		clustered_boxes, cluster_limits = cluster_boxes(boxes)
		subboxes, subboxmaps = split_boxmap(boxmap, clustered_boxes, cluster_limits)
		all_final_boxes = []
		for subboxes_item, subboxmaps_item, limits in zip(subboxes, subboxmaps, cluster_limits):
			[(x1, x2), (y1, y2)] = limits
			final_boxes = func(subboxes_item, subboxmaps_item)
			final_boxes[..., [0, 2]] += x1
			final_boxes[..., [1, 3]] += y1
			all_final_boxes.append(final_boxes)
		return np.concatenate(all_final_boxes, axis=0)
	return wrapper


