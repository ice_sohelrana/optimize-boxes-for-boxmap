import numpy as np
def clip_boxes(boxes,boxmap):
	# import pdb; pdb.set_trace()
	indices = np.where(boxes[:,0]<0)[0]
	boxes[indices,0:1]=0
	indices = np.where(boxes[:,1]<0)[0]
	boxes[indices,1:2]=0
	indices = np.where(boxes[:,2]>boxmap.shape[1])[0]
	boxes[indices,2:3]==boxmap.shape[1]
	indices = np.where(boxes[:,3]>boxmap.shape[0])[0]
	boxes[indices,3:4]==boxmap.shape[0]
	return boxes