import functools
import numpy as np

from utils.boxmap_represent import boxmapCreate
from utils.clustering import cluster_boxes, split_boxmap,with_clustering
from utils.cluster_resize import downsampling_cluster,upsampling_cluster

def seperator_lines(boxmap_shape,sub_cluster_width):
	"""
	Find out number of lines that seperate the sub_cluster
	"""   
	max_size=boxmap_shape[1]
	x = 0
	lines = []
	while (max_size-(x+sub_cluster_width))>=sub_cluster_width:
		x = x + sub_cluster_width
		lines.append([(x,0),(x,boxmap_shape[0])])
	return lines
def check_boxes_intersects_line(boxes,pt1,pt2):
	"""
	Check whether boxes intersects a line defined by two points 
	"""   
	intersected_box_indices = []
	for i,box in enumerate(boxes):
		if (pt1[0]<box[0] and pt2[0]<box[0])==False and (pt1[0]>box[2] and pt2[0]>box[2])==False :
			intersected_box_indices.append(i)
	return intersected_box_indices
def sub_cluster_partitioning(boxes,boxmaps,total_subcluster=2):
	"""
	Partition cluster vertically and find out the sub_cluster region for sub_cluster lines and partitions (untouched box regions) 
	"""
	# Define interval between two positioned lines
	sub_cluster_width = int(boxmaps.shape[1]/total_subcluster)
	# Define the position of each line that seperate each sub_cluster 
	lines = seperator_lines(boxmaps.shape,sub_cluster_width)
	all_intersected_box_indices = np.array([],dtype=np.int32)
	all_intersected_boxes=[]
	all_intersected_limits=[]
	# Find out the boxes and corresponding boxmap limits for each line
	for line in lines:
		intersected_box_indices = check_boxes_intersects_line(boxes,line[0],line[1])
		intersected_boxes=boxes[intersected_box_indices]
		x1,y1,x2,y2=int(round(min(intersected_boxes[:,0]))),int(round(min(intersected_boxes[:,1]))),int(round(max(intersected_boxes[:,2]))),int(round(max(intersected_boxes[:,3])))
		intersected_boxes[...,[0,2]]-=x1
		intersected_boxes[...,[1,3]]-=y1
		all_intersected_boxes.append(intersected_boxes)
		all_intersected_limits.append([(x1,x2),(y1,y2)])
		all_intersected_box_indices = np.concatenate((all_intersected_box_indices,intersected_box_indices),axis=0)
	# Find out the boxes that doesn't intersect with lines
	untouched_boxes = np.delete(boxes, all_intersected_box_indices, axis=0)
	return all_intersected_boxes,all_intersected_limits,untouched_boxes
def sub_clustering(boxes,boxmap,func,total_subcluster):
	"""
	Do optimization in sub_cluster lines and partitions (untouched box regions) 
	"""
	# Get all sub_cluster boxes and limits 
	intersected_boxes,intersected_limits,untouched_boxes = sub_cluster_partitioning(boxes,boxmap,total_subcluster=total_subcluster)
	remaining_boxmap = boxmap
	all_final_boxes = []
	# Optimize all sub_clusters that intersect with lines
	for intersected_boxes_item,intersected_limit_item in zip(intersected_boxes,intersected_limits):
		[(x1, x2), (y1, y2)] = intersected_limit_item
		intersected_boxmap = boxmap[y1:y2,x1:x2]
		final_intersected_boxes = func(intersected_boxes_item,intersected_boxmap)
		final_intersected_boxes[..., [0, 2]] += x1
		final_intersected_boxes[..., [1, 3]] += y1
		intersected_result_boxmap = boxmapCreate(final_intersected_boxes,boxmap.shape)
		# Subtract the resultant boxmap from boxmap
		remaining_boxmap = remaining_boxmap - intersected_result_boxmap
		all_final_boxes.append(final_intersected_boxes)
	# Optimize all sub_cluster that doesn't intersect with lines
	optimize_func = with_clustering(func)
	final_untouched_boxes = optimize_func(untouched_boxes,remaining_boxmap)
	all_final_boxes.append(final_untouched_boxes)
	all_final_boxes = np.concatenate(all_final_boxes, axis=0)
	return all_final_boxes

def with_sub_clustering(func):
	@functools.wraps(func)
	def wrapper(boxes, boxmap):
		clustered_boxes, cluster_limits = cluster_boxes(boxes)
		subboxes, subboxmaps = split_boxmap(boxmap, clustered_boxes, cluster_limits)
		all_final_boxes = []
		for subboxes_item, subboxmaps_item, limits in zip(subboxes, subboxmaps, cluster_limits):
			[(x1, x2), (y1, y2)] = limits
			subboxmaps_item,subboxes_item,downsampling_ratio = downsampling_cluster(subboxes_item,subboxmaps_item)
			if len(subboxes_item)<=50:
				final_boxes = func(subboxes_item,subboxmaps_item)
			elif len(subboxes_item)>50 and len(subboxes_item)<100:
				final_boxes = sub_clustering(subboxes_item,subboxmaps_item,func,total_subcluster=2)
			elif len(subboxes_item)>=100 and len(subboxes_item)<200:
				final_boxes = sub_clustering(subboxes_item,subboxmaps_item,func,total_subcluster=3)
			elif len(subboxes_item)>=200 and len(subboxes_item)<300:
				final_boxes = sub_clustering(subboxes_item,subboxmaps_item,func,total_subcluster=4)
			elif len(subboxes_item)>=300 and len(subboxes_item)<400:
				final_boxes = sub_clustering(subboxes_item,subboxmaps_item,func,total_subcluster=6)
			elif len(subboxes_item)>=400 and len(subboxes_item)<500:
				final_boxes = sub_clustering(subboxes_item,subboxmaps_item,func,total_subcluster=8)
			else:
				final_boxes = sub_clustering(subboxes_item,subboxmaps_item,func,total_subcluster=10)
			final_boxes = upsampling_cluster(final_boxes,downsampling_ratio)
			final_boxes[..., [0, 2]] += x1
			final_boxes[..., [1, 3]] += y1
			all_final_boxes.append(final_boxes)
		return np.concatenate(all_final_boxes, axis=0)
	return wrapper