import numpy as np
import torch
import torchvision.ops
from torch import Tensor
from utils.boxmap_represent import boxmapCreate

def calc_iou(bboxes1: np.ndarray, bboxes2: np.ndarray, is_pixel_box: bool = True):
	"""Calculate the intersection over union values between two sets of boxes.

	Parameters
	----------
	bboxes1 : (M, 4) ndarray
	bboxes2 : (N, 4) ndarray
	is_pixel_box : bool
		If True, assume that the bounding box coordinates correspond to a pixel grid, and that the right and bottom
		edges of the box are inclusive. That is, the box (0, 0, 0, 0) contains 1 pixel.

	Returns
	-------
		ious : (M, N) ndarray
	"""
	assert bboxes1.shape[-1] == 4
	assert bboxes2.shape[-1] == 4
	x11, y11, x12, y12 = np.split(bboxes1, 4, axis=-1)
	x21, y21, x22, y22 = np.split(bboxes2, 4, axis=-1)
	xA = np.maximum(x11, np.transpose(x21))
	yA = np.maximum(y11, np.transpose(y21))
	xB = np.minimum(x12, np.transpose(x22))
	yB = np.minimum(y12, np.transpose(y22))
	c = 1 if is_pixel_box else 0
	interArea = np.maximum((xB - xA + c), 0) * np.maximum((yB - yA + c), 0)
	boxAArea = (x12 - x11 + c) * (y12 - y11 + c)
	boxBArea = (x22 - x21 + c) * (y22 - y21 + c)
	iou = interArea / (boxAArea + np.transpose(boxBArea) - interArea)
	return iou

def boxExpansion(box1,box2):
	x1 = min(box1[0],box2[0])
	y1 = min(box1[1],box2[1])
	x2 = max(box1[2],box2[2])
	y2 = max(box1[3],box2[3])
	box = [x1,y1,x2,y2]
	return box

def eval_fnc(box_combs,given_boxmap,init_boxmap):
	pred_boxmap = np.zeros(given_boxmap.shape)
	if init_boxmap is not None:
		pred_boxmap[:] = init_boxmap
	if len(box_combs) > 0:
		pred_boxmap = boxmapCreate(np.stack(box_combs), given_boxmap.shape, pred_boxmap)
	cost = np.linalg.norm(given_boxmap - pred_boxmap)
	return cost, pred_boxmap

@torch.jit.script
def nms(boxes, scores, iou_threshold):
	# type: (Tensor, Tensor, float) -> Tensor
	"""
	Performs non-maximum suppression (NMS) on the boxes according
	to their intersection-over-union (IoU).
	NMS iteratively removes lower scoring boxes which have an
	IoU greater than iou_threshold with another (higher scoring)
	box.
	If multiple boxes have the exact same score and satisfy the IoU
	criterion with respect to a reference box, the selected box is
	not guaranteed to be the same between CPU and GPU. This is similar
	to the behavior of argsort in PyTorch when repeated values are present.
	Parameters
	----------
	boxes : Tensor[N, 4])
		boxes to perform NMS on. They
		are expected to be in (x1, y1, x2, y2) format
	scores : Tensor[N]
		scores for each one of the boxes
	iou_threshold : float
		discards all overlapping
		boxes with IoU > iou_threshold
	Returns
	-------
	keep : Tensor
		int64 tensor with the indices
		of the elements that have been kept
		by NMS, sorted in decreasing order of scores
	"""
	return torchvision.ops.nms(boxes, scores, iou_threshold)

@torch.jit.script
def batched_nms(boxes, scores, idxs, iou_threshold):
	# type: (Tensor, Tensor, Tensor, float) -> Tensor
	"""
	Performs non-maximum suppression in a batched fashion.
	Each index value correspond to a category, and NMS
	will not be applied between elements of different categories.
	Parameters
	----------
	boxes : Tensor[N, 4]
		boxes where NMS will be performed. They
		are expected to be in (x1, y1, x2, y2) format
	scores : Tensor[N]
		scores for each one of the boxes
	idxs : Tensor[N]
		indices of the categories for each one of the boxes.
	iou_threshold : float
		discards all overlapping boxes
		with IoU > iou_threshold
	Returns
	-------
	keep : Tensor
		int64 tensor with the indices of
		the elements that have been kept by NMS, sorted
		in decreasing order of scores
	"""
	if boxes.numel() == 0:
		return torch.empty((0,), dtype=torch.int64, device=boxes.device)
	# strategy: in order to perform NMS independently per class.
	# we add an offset to all the boxes. The offset is dependent
	# only on the class idx, and is large enough so that boxes
	# from different classes do not overlap
	else:
		max_coordinate = boxes.max()
		offsets = idxs.to(boxes) * (max_coordinate + torch.tensor(1).to(boxes))
		boxes_for_nms = boxes + offsets[:, None]
		keep = nms(boxes_for_nms, scores, iou_threshold)
		return keep
