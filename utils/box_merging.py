import numpy as np
import functools

from utils.utils import calc_iou
def iou_merged_boxes(boxes,iou_threshold=0.95):	
	boxes_ious = calc_iou(boxes[:,0:4],boxes[:,0:4])
	merged_box_indices = []
	for box_iou in boxes_ious:
		indices=np.where(box_iou>iou_threshold)[0]
		boxes_ious[:,indices]=0
		if indices!=[]:
			merged_box_indices.append(indices)
	merged_indices = []
	merged_boxes = []
	for merged_indice in merged_box_indices:
		if boxes.shape[1]!=7:
			merged_boxes.append([boxes[merged_indice][:,0].mean(),boxes[merged_indice][:,1].mean(),boxes[merged_indice][:,2].mean(),boxes[merged_indice][:,3].mean(),max(boxes[merged_indice][:,4])])
		else:
			merged_boxes.append([boxes[merged_indice][:,0].mean(),boxes[merged_indice][:,1].mean(),boxes[merged_indice][:,2].mean(),boxes[merged_indice][:,3].mean(),max(boxes[merged_indice][:,4]),0,0])
		merged_indices.extend(merged_indice)
	merged_boxes = np.array(merged_boxes)
	all_indices = [i for i in range(len(boxes))]
	remaining_indices = list(set(all_indices)-set(merged_indices))
	remaining_boxes=boxes[remaining_indices]
	if len(merged_boxes)!=0 and len(remaining_boxes)!=0:
		final_boxes = np.concatenate((merged_boxes,remaining_boxes),axis=0)
	if len(merged_boxes)==0:
		final_boxes = remaining_boxes
	if len(remaining_boxes)==0:
		final_boxes = merged_boxes
	return final_boxes
def with_box_merging(boxes,iou_threshold):
	def box_merging_impl(func):
		@functools.wraps(func)
		def wrapper(boxes, boxmap):
			new_boxes=iou_merged_boxes(boxes,iou_threshold)
			final_boxes = func(new_boxes, boxmap)
			return final_boxes
		return wrapper
	return box_merging_impl