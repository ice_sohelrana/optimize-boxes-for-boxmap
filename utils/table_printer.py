class TablePrinter:
	def __init__(self, table_columns):
		self.table_columns = table_columns
		self.col_sep = '  '

	@property
	def headers(self):
		return [e[0] for e in self.table_columns]

	@property
	def column_widths(self):
		return [e[1] for e in self.table_columns]

	@property
	def formatters(self):
		return [e[2] for e in self.table_columns]

	@property
	def n_columns(self):
		return len(self.table_columns)

	def _print_row(self, row):
		row = [s[:width].rjust(width) for s, width in zip(row, self.column_widths)]
		print(self.col_sep.join(row))

	def print_header(self):
		self._print_row(self.headers)

		self._print_row(['-' * width for width in self.column_widths])

	def print_data_row(self, *args):
		if len(args) != self.n_columns:
			raise ValueError('expected {} arguments, got {}'.format(self.n_columns, len(args)))
		self._print_row([
			format_func(value)
			for value, format_func in zip(args, self.formatters)
		])
