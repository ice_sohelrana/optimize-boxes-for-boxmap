This project is tasked to find an efficient optimization algorithm for the problem. 


## Problem definition

Given bounding boxes (generated from a detection algorithm) and a boxmap, optimization algorithm need to determine best bounding boxes combination which minimize the score in boxmap. A boxmap is a 2-d image map where each pixel value is determined by the number of objects at that place (shown in figure 1).

<div align="center"><img src="./imgs/boxmap.png" alt="Boxmap representation" width="40%"></div>
<div align="center"><strong>Figs 1. Boxmap representation (pixel value represents number of overlapped boxes)</strong></div>

<div align="center"><img src="./imgs/problem_definition.png" width="100%" alt="Problem definition"></div>
<div align="center"><strong>Figs 2. Problem definition: A box optimization algorithm which takes boxes and a boxmap as input to find optimum number of boxes</strong></div>


## Description

As an input we have boxes and a given boxmap (GT_boxmap)
Every time we choose a combination of boxes from all box combination and create boxmap for that combination. Then we estimate euclidean distance between created boxmap and our given boxmap (GT_boxmap). For which boxes combination it produce minimum score is determined as optimum boxes for the boxmap (GT_boxmap).

### A common solution

```
for a_box_combination in all_boxes_combination:
    created_boxmap = create_boxmap(a_box_combination)
    score = dist(created_boxmap,GT_boxmap)
    if score<min_score:
        min_score = score
        best_boxes = a_box_combination
``` 


## Looking for an efficient optimization algorithm

We don't find efficient solution to find best boxes which fits to our given boxmap (GT_boxmap). Let there are n number of boxes, then we need to compare C(n,1)+C(n,2)+...C(n,n) combination of boxes which is pretty high number of comparison particularly when the value of n is very high.

### How to run

```python optimize.py --data_path='./dataset' --boxmap_type=gt_boxmap --opt_algorithm=nms```
