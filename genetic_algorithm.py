import numpy as np
import pygad
import torch
from aabbtree import AABB, AABBTree

from pixel_voting import pixel_voting_indices
from utils.boxmap_represent import boxmapCreate
from utils.utils import batched_nms


def _genetic_algorithm_impl(boxes, boxmap, initial_population=None, num_generations=100):
	def fitness_func(solution, solution_idx):
		proposed_boxes = boxes[solution.astype(np.bool)]
		proposed_boxmap = boxmapCreate(proposed_boxes, boxmap.shape)
		return -np.abs(boxmap - proposed_boxmap).mean()

	ga = pygad.GA(
		num_generations=num_generations,
		num_parents_mating=2,
		fitness_func=fitness_func,
		sol_per_pop=6,
		num_genes=len(boxes),
		gene_space=[0, 1],
		parent_selection_type='sss',
		keep_parents=-1,
		crossover_type='single_point',
		mutation_type='random',
		mutation_num_genes=1,
		mutation_by_replacement=True,
		initial_population=initial_population,
	)

	ga.run()

	# ga.plot_result()
	# exit(0)

	solution, fitness, _ = ga.best_solution()

	return solution, ga.population


def aabb_tree_to_value_list(tree: AABBTree):
	l = []
	# Preorder traversal
	if tree.value:
		l.append(tree.value)
	if tree.left:
		l.extend(aabb_tree_to_value_list(tree.left))
	if tree.right:
		l.extend(aabb_tree_to_value_list(tree.right))
	return l


def _argsort_boxes(boxes):
	"""Spatially sort boxes using an AABB tree.
	"""
	tree = AABBTree()
	for index, (x1, y1, x2, y2, confidence) in enumerate(boxes):
		tree.add(AABB([(x1, x2), (y1, y2)]), index)
	spatial_ordering = aabb_tree_to_value_list(tree)
	return spatial_ordering


def _initialise_genes_nms(boxes, boxmap, threshold=0.5):
	orig_inds = np.where(boxes[:, 4] >= threshold)[0]
	torch_boxes = torch.from_numpy(boxes[orig_inds])
	labels = torch.ones(len(torch_boxes))
	nms_keep_indices = np.asarray(batched_nms(torch_boxes[:, 0:4], torch_boxes[:, 4], labels, iou_threshold=0.5))
	nms_keep = np.zeros(len(boxes))
	nms_keep[orig_inds[nms_keep_indices]] = 1
	return nms_keep


def _initialise_genes_pixel_voting(boxes, boxmap, threshold=0.5):
	orig_inds = np.where(boxes[:, 4] >= threshold)[0]
	keep_indices = pixel_voting_indices(boxes[orig_inds], boxmap)
	keep = np.zeros(len(boxes))
	keep[orig_inds[keep_indices]] = 1
	return keep


def genetic_algorithm(boxes, boxmap):
	# Order boxes spatially so that genetic crossover makes logical sense.
	boxes = boxes[_argsort_boxes(boxes)]
	# Get initial genes.
	pv_genes = _initialise_genes_pixel_voting(boxes, boxmap)
	nms_genes = _initialise_genes_nms(boxes, boxmap)
	# Run the genetic algorithm.
	genes, _ = _genetic_algorithm_impl(boxes, boxmap, initial_population=[pv_genes] * 3 + [nms_genes] * 3, num_generations=100)
	# Return the selected boxes.
	return boxes[genes.astype(np.bool)]
