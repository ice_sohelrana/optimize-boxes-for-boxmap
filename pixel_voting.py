import numpy as np

from utils.boxmap_represent import boxmapCreate
from utils.utils import eval_fnc


def pixel_voting_indices(boxes, boxmap, best_cost=None, selected_box_indices=None, pred_boxmap=None, remaining_boxes=None):
	"""A greedy approach based on 'voting' from the boxmap pixels.
	"""
	selected_indices = []
	if best_cost==None:
		# Calculate the starting cost (no boxes selected).
		best_cost, pred_boxmap = eval_fnc([], boxmap, None)
		# `remaining_boxes` tracks all boxes that have not yet been selected.
		remaining_boxes = np.asarray(boxes, dtype=np.int32)
	else:
		# nms initiated selected
		selected_indices.extend(selected_box_indices)
	remaining_indices = np.setdiff1d(np.arange(len(boxes)), selected_indices)
	remaining_boxmap = boxmapCreate(remaining_boxes, boxmap.shape)
	# Main optimisation loop.
	while len(remaining_boxes) > 0:
		# Remove `selected_boxes` from the original boxmap.
		cur_boxmap = boxmap - pred_boxmap
		# Calculate scores for each remaining box using pixel voting.
		with np.errstate(divide='ignore', invalid='ignore'):
			vote_map = cur_boxmap / remaining_boxmap
		scores = [vote_map[y1:y2, x1:x2].mean() for x1, y1, x2, y2, _ in remaining_boxes]
		# Select the best candidate from the remaining boxes.
		candidate_index = np.argmax(scores)
		candidate_box = remaining_boxes[candidate_index]
		# Evaluate the updated cost if we were to include the candidate box.
		pred_boxmap[candidate_box[1]:candidate_box[3], candidate_box[0]:candidate_box[2]] += 1
		cost = np.linalg.norm(boxmap - pred_boxmap)
		if cost < best_cost:
			# Update the best cost.
			best_cost = cost
			# Add the candidate box to the selected boxes.
			selected_indices.append(remaining_indices[candidate_index])
			# Remove the candidate box from the remaining boxes.
			remaining_boxes = np.delete(remaining_boxes, candidate_index, axis=0)
			remaining_indices = np.delete(remaining_indices, candidate_index, axis=0)
			remaining_boxmap[candidate_box[1]:candidate_box[3], candidate_box[0]:candidate_box[2]] -= 1
		else:
			# If including the candidate box does not improve results, stop optimising.
			break
	# Return the selected indices.
	return selected_indices


def pixel_voting(boxes, boxmap, best_cost=None, selected_box_indices=None, pred_boxmap=None, remaining_boxes=None):
	"""A greedy approach based on 'voting' from the boxmap pixels.
	"""
	selected_indices = pixel_voting_indices(boxes, boxmap, best_cost, selected_box_indices, pred_boxmap, remaining_boxes)
	return boxes[selected_indices]


def _multipath_pixel_voting_impl(selected_box_indices, boxes, boxmap, k, best_cost, pred_boxmap,
		remaining_boxmap, cache):
	cache_key = tuple(sorted(selected_box_indices))
	if cache_key in cache:
		return cache[cache_key]
	options = [(best_cost, selected_box_indices)]
	remaining_boxes = np.delete(boxes, selected_box_indices, axis=0)
	remaining_box_indices = np.delete(np.arange(len(boxes)), selected_box_indices, axis=0)
	if len(remaining_boxes) > 0:
		# Remove selected boxes from the original boxmap.
		cur_boxmap = boxmap - pred_boxmap
		# Calculate scores for each remaining box using pixel voting.
		with np.errstate(divide='ignore', invalid='ignore'):
			vote_map = cur_boxmap / remaining_boxmap
		scores = [vote_map[y1:y2, x1:x2].mean() for x1, y1, x2, y2 in remaining_boxes]
		# Select the top k best candidates from the remaining boxes.
		candidate_indices = remaining_box_indices[np.argsort(scores)[::-1][:k]]
		for candidate_index in candidate_indices:
			candidate_box = boxes[candidate_index]
			# Evaluate the updated cost if we were to include the candidate box.
			sub_pred_boxmap = pred_boxmap.copy()
			sub_pred_boxmap[candidate_box[1]:candidate_box[3], candidate_box[0]:candidate_box[2]] += 1
			# sub_cost = np.linalg.norm(boxmap - sub_pred_boxmap)
			sub_cost = abs(boxmap-sub_pred_boxmap).mean()
			if sub_cost < best_cost:
				sub_selected_box_indices = [*selected_box_indices, candidate_index]
				sub_remaining_boxmap = remaining_boxmap.copy()
				sub_remaining_boxmap[candidate_box[1]:candidate_box[3], candidate_box[0]:candidate_box[2]] -= 1
				options.append(_multipath_pixel_voting_impl(sub_selected_box_indices, boxes, boxmap, k,
					sub_cost, sub_pred_boxmap, sub_remaining_boxmap, cache))
	result = (sorted(options, key=lambda t: t[0])[0])
	cache[cache_key] = result
	return result


def multipath_pixel_voting(boxes, boxmap, best_cost=None, selected_box_indices=None, pred_boxmap=None, remaining_boxes=None, k=2):
	"""A variant of pixel voting which considers multiple "paths" of box selection.

	Calling multipath_pixel_voting with k=1 should be equivalent to pixel_voting (but the performance
	is slightly less optimised).
	"""
	# Calculate initial state.
	
	if best_cost==None:
		best_cost, pred_boxmap = eval_fnc([], boxmap, None)
		selected_box_indices = []
		remaining_boxmap = boxmapCreate(boxes, boxmap.shape)
	else:
		remaining_boxmap = boxmapCreate(remaining_boxes, boxmap.shape)
	inp_boxes = np.asarray(boxes[:,0:4], dtype=np.int32)
	# Run the algorithm.
	cost, selected_box_indices = _multipath_pixel_voting_impl(
		selected_box_indices, inp_boxes, boxmap, k, best_cost, pred_boxmap, remaining_boxmap, {})
	# Return the selected boxes.
	return boxes[selected_box_indices]
