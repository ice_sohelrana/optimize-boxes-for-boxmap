from utils.clustering import cluster_boxes, split_boxmap, with_clustering
from utils.plotting import plot_boxmap
import numpy as np
from numpy.testing import assert_equal


def test_cluster_boxes_1(boxes_1, gt_boxmap_1):
	clustered_boxes, cluster_limits = cluster_boxes(boxes_1)
	# for boxes in clustered_boxes:
	# 	plot_boxmap(gt_boxmap_1, boxes).show()
	# Check that all boxes in boxes_1 are in subboxes.
	assert_equal(np.sort(np.concatenate(clustered_boxes), axis=0),
				 np.sort(boxes_1, axis=0))
	# Check that we have the correct number of clusters.
	assert len(clustered_boxes) == 7
	assert len(cluster_limits) == 7


def test_cluster_boxes_5(boxes_5, gt_boxmap_5):
	clustered_boxes, cluster_limits = cluster_boxes(boxes_5)
	# for boxes in clustered_boxes:
	# 	plot_boxmap(gt_boxmap_5, boxes).show()
	# Check that all boxes in boxes_5 are in subboxes.
	assert_equal(np.sort(np.concatenate(clustered_boxes), axis=0),
				 np.sort(boxes_5, axis=0))
	# Check that we have the correct number of clusters.
	assert len(clustered_boxes) == 10
	assert len(cluster_limits) == 10


def test_split_into_clusters(boxes_1, gt_boxmap_1):
	# plot_boxmap(gt_boxmap_1, boxes_1).show()
	clustered_boxes, cluster_limits = cluster_boxes(boxes_1)
	subboxes, subboxmaps = split_boxmap(gt_boxmap_1, clustered_boxes, cluster_limits)
	assert len(subboxes) == 7
	assert len(subboxmaps) == 7
	# for a, b in zip(subboxmaps, subboxes):
	# 	plot_boxmap(a, b).show()


def test_with_clustering(boxes_1, gt_boxmap_1):
	call_count = 0
	@with_clustering
	def identity_optimize(boxes, boxmaps):
		nonlocal call_count
		call_count += 1
		return boxes[..., :4]
	final_boxes = identity_optimize(boxes_1, gt_boxmap_1)
	assert_equal(np.sort(final_boxes, axis=0),
				 np.sort(boxes_1[..., :4], axis=0))
	assert call_count == 7
