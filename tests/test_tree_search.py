from tree_search import powerset


def test_powerset():
	items = ['a', 'b', 'c', 'd']
	expected = [
		tuple(), ('a',), ('b',), ('c',), ('d',), ('a', 'b'), ('a', 'c'), ('a', 'd'), ('b', 'c'), ('b', 'd'), ('c', 'd'),
		('a', 'b', 'c'), ('a', 'b', 'd'), ('a', 'c', 'd'), ('b', 'c', 'd'), ('a', 'b', 'c', 'd'),
	]
	actual = list(powerset(items))
	assert actual == expected


def test_powerset_depth_2():
	items = ['a', 'b', 'c', 'd']
	expected = [
		tuple(), ('a',), ('b',), ('c',), ('d',), ('a', 'b'), ('a', 'c'), ('a', 'd'), ('b', 'c'), ('b', 'd'), ('c', 'd'),
	]
	actual = list(powerset(items, depth=2))
	assert actual == expected
