from pathlib import Path

import numpy as np
import pytest


@pytest.fixture
def dataset_path():
	path = Path(__file__).parent.parent.joinpath('dataset').absolute()
	return path


@pytest.fixture
def gt_boxmap_1(dataset_path):
	boxmap = np.load(dataset_path.joinpath('gt_boxmap', '1.npy'))
	return boxmap


@pytest.fixture
def boxes_1(dataset_path):
	boxes = np.load(dataset_path.joinpath('pred_boxes', '1.npy'))
	return boxes


@pytest.fixture
def gt_boxmap_5(dataset_path):
	boxmap = np.load(dataset_path.joinpath('gt_boxmap', '5.npy'))
	return boxmap


@pytest.fixture
def boxes_5(dataset_path):
	boxes = np.load(dataset_path.joinpath('pred_boxes', '5.npy'))
	return boxes
