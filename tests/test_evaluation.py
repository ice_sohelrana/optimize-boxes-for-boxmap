import numpy as np
from numpy.testing import assert_allclose
import pytest

from utils.evaluation import match_boxes, calc_ap, calc_precision


@pytest.fixture
def pred_boxes():
	return np.asarray([
		[449.5855712890625, 426.0487365722656, 538.546630859375,  655.8604125976562,  0.9999985694885254],
		[621.58984375,      260.5461730957031, 708.9006958007812, 466.11053466796875, 0.999997615814209],
		[621.5397338867188, 260.5880432128906, 708.5589599609375, 466.95172119140625, 0.9999972581863403],
		[621.792724609375,  260.0928649902344, 708.4435424804688, 466.4792785644531,  0.9999967813491821],
		[1316.169677734375, 385.9031066894531, 1432.004638671875, 594.7135620117188,  0.9999963045120239],
		[449.5358581542969, 423.900146484375,  538.1228637695312, 659.0731811523438,  0.9999958276748657],
		[449.4879150390625, 424.5154113769531, 538.0680541992188, 657.9019165039062,  0.9999954700469971],
	])


@pytest.fixture
def gt_boxes():
	return np.asarray([
		[343.0,  463.0, 417.0,  665.0],
		[450.0,  427.0, 537.0,  657.0],
		[621.0,  260.0, 708.0,  467.0],
		[835.0,  23.0,  905.0,  178.0],
		[1088.0, 107.0, 1138.0, 273.0],
	])


@pytest.fixture
def expected_matches():
	return [
		# (gt_index, pred_index, iou)
		(1, 0, 0.96948725),
		(2, 1, 0.9765155),
	]


def test_match_boxes(pred_boxes, gt_boxes, expected_matches):
	match_results = match_boxes(pred_boxes[:, :4], gt_boxes, iou_threshold=0.5, policy='coco')
	actual_matches = []
	for i, j, iou in zip(*match_results):
		actual_matches.append((j, i, iou))
	assert_allclose(np.asarray(sorted(actual_matches)), np.asarray(sorted(expected_matches)))


def test_calc_ap(pred_boxes, gt_boxes):
	actual_ap = calc_ap([pred_boxes], [gt_boxes], iou_threshold=0.5, policy='coco')
	assert actual_ap == pytest.approx(0.4, abs=1e-6)


def test_ignored():
	ignored_gt_indices = [0]
	gt_boxes = np.asarray([
		[0, 0, 100, 100],     # Ignored
		[0, 0, 50, 50],       # Not ignored
		[500, 500, 550, 550], # Not ignored
	])
	pred_boxes = np.asarray([
		[101, 101, 200, 200, 0.99],  # Skipped (matches ignored GT)
		[0, 0, 100, 100, 0.98],      # Skipped (matches ignored GT)
		[99, 99, 200, 200, 0.97],    # FP
		[0, 0, 50, 50, 0.96],        # TP
		[0, 0, 50, 50, 0.95],        # Skipped (matches ignored GT)
		[500, 500, 550, 550, 0.94],  # TP
		[490, 490, 550, 550, 0.93],  # FP (duplicate)
		[500, 500, 560, 570, 0.92],  # FP (duplicate)
	])
	actual_precision = calc_precision([pred_boxes], [gt_boxes], [ignored_gt_indices])
	expected_precision = 2 / 5  # TP / (TP + FP)
	assert actual_precision == pytest.approx(expected_precision, abs=1e-6)
