import numpy as np
from numpy.testing import assert_allclose

pred_boxes = np.asarray([
	[10.0, 20.0, 30.0, 40.0],
	[10.0, 50.0, 30.0, 60.0],
])

gt_boxes = np.asarray([
	[10.0, 20.0, 30.0, 40.0],
	[10.0, 20.0, 30.0, 60.0],
	[10.0, 30.0, 30.0, 40.0],
])

expected_ious = np.asarray([
	[1.0, 0.5, 0.5],
	[0.0, 0.25, 0.0],
])


def test_calc_iou():
	from utils.utils import calc_iou
	ious = calc_iou(pred_boxes, gt_boxes, is_pixel_box=False)
	assert_allclose(ious, expected_ious)
