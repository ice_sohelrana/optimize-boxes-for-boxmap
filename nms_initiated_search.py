import numpy as np
from numpy import *
import itertools

from nms import nms_optimize
from utils.utils import eval_fnc
from utils.boxmap_represent import boxmapCreate

def powerset(items, depth=None):
	"""Return all subsets of `items`.

	Parameters
	----------
	items : list
		The universal set.
	depth : int
		If specified, the maximum length of returned subsets is limited to the value of `depth`.
	"""
	if depth is None:
		depth = len(items)
	for m in range(depth + 1):
		for b in itertools.combinations(items, m):
			yield b

def nms_initiated_search(boxes, given_box_map,best_cost,best_indices,best_boxmap,remaining_boxes,depth=2):
	cur_min_cost = best_cost
	cur_boxmap = best_boxmap
	cur_boxes = boxes[best_indices]
	cur_boxes = [*cur_boxes]
	for comb_idxs in powerset(list(range(len(remaining_boxes))), depth=depth):
		# Collect the combination of boxes
		comb = [remaining_boxes[c] for c in comb_idxs]
		# Add collected boxes boxmap with current boxmap and do cost computation
		cost,out_boxmap = eval_fnc(comb, given_box_map,cur_boxmap)
		if cost <= cur_min_cost:
			# Update the best cost
			cur_min_cost = cost
			# Save added box combiations
			new_boxes = cur_boxes+comb
			new_boxmap = out_boxmap
	if len(new_boxes) > 0:
		result = np.stack(new_boxes)
	else:
		result = np.empty((0, 5))
	return result
def nms_initiated_greedy_search(boxes,given_box_map,best_cost,best_indices,best_boxmap,remaining_boxes,k=1,depth=2):
	# Best boxes until now (best boxes from nms initated search)
	global_best_boxes = []
	global_best_cost = []
	global_best_cost.append(best_cost)
	global_best_boxes.append(boxes[best_indices])

	# Start:- -----Add a box from remaining boxes and find out k best local boxes 
	# Iniate best boxes as current boxes
	cur_boxmap = best_boxmap
	cur_boxes = boxes[best_indices]
	cur_boxes = [*cur_boxes]
	cur_min_cost = best_cost
	local_best_costs = []
	local_best_boxes = {}
	local_best_boxmaps = {}
	for indices,comb in enumerate(remaining_boxes):
		cost,out_boxmap = eval_fnc([comb], given_box_map,cur_boxmap)
		local_best_costs.append(cost)
		local_best_boxes[indices] = cur_boxes+[comb]
		local_best_boxmaps[indices] = out_boxmap
	k_costs_indices = np.argsort(np.stack(local_best_costs))[:k]
	# End:- -----Add a box from remaining boxes and find out k best local boxes 

	# Append the best boxes from above search
	global_best_cost.append(local_best_costs[k_costs_indices[0]])
	global_best_boxes.append(local_best_boxes[k_costs_indices[0]])

	# Start:- -----Add a box in each k boxes and find out k best local boxes
	for indice in k_costs_indices:
		cur_boxes = local_best_boxes[indice]
		cur_boxmap = local_best_boxmaps[indice]
		new_local_best_costs = []
		new_local_best_boxes = {}
		new_local_best_boxmaps = {}
		new_remaining_boxes = np.delete(remaining_boxes,indice,axis=0)
		for indices,comb in enumerate(new_remaining_boxes):
			cost,out_boxmap = eval_fnc([comb], given_box_map,cur_boxmap)
			new_local_best_costs.append(cost)
			new_local_best_boxes[indices] = cur_boxes+[comb]
			new_local_best_boxmaps[indices] = out_boxmap
		new_k_costs_indices = np.argsort(np.stack(new_local_best_costs))[:k]
		# Add best boxes from above search with global best boxes
		global_best_cost.append(new_local_best_costs[new_k_costs_indices[0]])
		global_best_boxes.append(new_local_best_boxes[new_k_costs_indices[0]])
	# End:- -----Add a box in each k boxes and find out k best local boxes
	# Find out the best box combination from all global best boxes combination
	best_indice = np.argmin(np.array(global_best_cost))
	result = global_best_boxes[best_indice]
	if len(result) > 0:
		result = np.stack(result)
	else:
		result = np.empty((0, 5))
	return result