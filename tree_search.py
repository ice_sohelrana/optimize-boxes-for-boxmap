import itertools

import numpy as np

import nms
from utils.utils import eval_fnc


def powerset(items, depth=None):
	"""Return all subsets of `items`.

	Parameters
	----------
	items : list
		The universal set.
	depth : int
		If specified, the maximum length of returned subsets is limited to the value of `depth`.
	"""
	if depth is None:
		depth = len(items)
	for m in range(depth + 1):
		for b in itertools.combinations(items, m):
			yield b

def tree_search(boxes, given_box_map, depth=2):
	boxes = [*boxes]
	result = []
	curr_cost=np.inf
	curr_boxmap = np.zeros((given_box_map.shape))
	while len(result) < len(boxes):
		# Evaluate current combination of boxes
		new_min_cost = np.inf
		new_boxmap = None
		new_box_idxs = []
		new_boxes = []
		# For each node in the search depth
		for comb_idxs in powerset(list(range(len(boxes))), depth=depth):
			# Collect the combination of additional boxes that minimises the cost
			comb = [boxes[c] for c in comb_idxs]
			cost,out_boxmap = eval_fnc(comb, given_box_map,curr_boxmap)
			if cost <= new_min_cost:
				new_min_cost = cost
				new_box_idxs = comb_idxs
				new_boxes = result+comb
				new_boxmap = out_boxmap
		# If no option in the search depth improve things, we're done here
		if new_min_cost >= curr_cost:
			break
		# Else, we have a new base point to search from
		result = new_boxes
		curr_cost = new_min_cost
		curr_boxmap = new_boxmap
		# Remove picked up boxes from being picked again later
		for idx in sorted(new_box_idxs, reverse=True):
			del boxes[idx]
	if len(result) > 0:
		result = np.stack(result)
	else:
		result = np.empty((0, 5))
	return result
