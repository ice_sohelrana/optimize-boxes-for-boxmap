import os
import time
from pathlib import Path
import numpy as np

from utils.evaluation import calc_ap, calc_lrp, calc_optimal_lrp
from nms import nms_optimize,with_nms,set_cpu_nms
from nms_initiated_search import nms_initiated_search,nms_initiated_greedy_search
from genetic_algorithm import genetic_algorithm
from brute_force_optimization import brute_force_optimize
from tree_search import tree_search
from pixel_voting import pixel_voting, multipath_pixel_voting
from utils.boxmap_represent import boxmapCreate
from utils.box_clip import clip_boxes
from utils.cluster_resize import with_max_image_size
from utils.box_merging import with_box_merging
from utils.clustering import with_clustering
from utils.sub_clustering import with_sub_clustering
from utils.nms_initiated_boxes import with_nms_initialization
from utils.plotting import plot_boxmap
from utils.table_printer import TablePrinter


# Here we have the dictionary containing all of the possible box optimization algorithms.
# Each function is expected to take exactly two arguments, `boxes` and `boxmap`, and return
# `final_boxes`.
# * `boxes` is an N x 5 numpy array of bounding box candidates: (x1, y1, x2, y2, confidence).
# * `boxmap` is a H x W numpy array containing the boxmap.
# * `final_boxes` is an N x 4 numpy array of refined bounding boxes: (x1, y1, x2, y2).
ALGORITHMS = {
	'brute-force-optimization': brute_force_optimize,
	'nms': nms_optimize,
	'set-nms': set_cpu_nms,
	'tree-search' : tree_search,
	'nms-tree-search' : lambda boxes,boxmap : with_nms(tree_search,threshold=0.85)(boxes,boxmap),
	'pixel-voting': pixel_voting,
	'multipath-pixel-voting': multipath_pixel_voting,
	'nms-initiated-search': nms_initiated_search,
	'nms-initiated-greedy-search': nms_initiated_greedy_search,
	'genetic-algorithm': genetic_algorithm,
	'identity': lambda boxes, boxmap: boxes,
	'null': lambda boxes, boxmap: np.empty((0, 4)),
}


def main(args):

	box_dirs = sorted(os.listdir(os.path.join(args.data_path, "pred_boxes")))
	boxmap_dirs = sorted(os.listdir(os.path.join(args.data_path, args.boxmap_type)))
	begin_time = time.time()
	images_boxes = []
	gt_boxes = []
	img_info = []
	printer = TablePrinter([
		('', 3, lambda x: '{:3d}'.format(x)),
		('Boxes in', 10, lambda x: '{:4d}'.format(x)),
		('Boxes out', 10, lambda x: '{:4d}'.format(x)),
		('Time (s)', 11, lambda x: '{:0.6f}'.format(x)),
		('MAE', 11, lambda x: '{:0.8f}'.format(x)),
		('LRP (s=0)', 11, lambda x: '{:0.8f}'.format(x)),
	])
	printer.print_header()
	for inds,(boxmap,boxes) in enumerate(zip(boxmap_dirs,box_dirs)):
		# Load a boxmap and corresponding predicted boxes
		gt_box = np.load(os.path.join(args.data_path, "gt_boxes",boxes))
		boxmap = np.load(os.path.join(args.data_path, args.boxmap_type,boxmap))
		boxes = np.load(os.path.join(args.data_path, "pred_boxes",boxes))
		# Use score threshold .01 for set-nms
		if args.low_score_boxes_prune=='Yes':
			boxes = boxes[boxes[:,4]>.65]
		if args.opt_algorithm!='nms' and args.opt_algorithm!='set-nms':
			boxes = clip_boxes(boxes,boxmap)
		if args.opt_algorithm!='set-nms':
			boxes=boxes[:,0:5]
		# Write optimization algorithm here to optimize boxes for the boxmap
		optimize_func = ALGORITHMS[args.opt_algorithm]
		if args.with_nms_initialization == 'Yes' and args.opt_algorithm!='nms' and args.opt_algorithm!='set-nms':
			optimize_func = with_nms_initialization(optimize_func)
		if args.with_clustering == 'Yes' and args.opt_algorithm!='nms' and args.opt_algorithm!='set-nms':
			optimize_func = with_clustering(optimize_func)
		if args.with_sub_clustering == 'Yes' and args.opt_algorithm!='nms' and args.opt_algorithm!='set-nms':
			optimize_func = with_sub_clustering(optimize_func)
		if args.with_box_merging=='Yes':
			optimize_func = with_box_merging(boxes,args.merging_iou_thresh)(optimize_func)
		if args.with_max_image_size > 0:
			optimize_func = with_max_image_size(args.with_max_image_size)(optimize_func)
		start_time = time.time()
		final_boxes = optimize_func(boxes, boxmap)
		final_boxmap = boxmapCreate(final_boxes, boxmap.shape)

		mae = np.abs(boxmap - final_boxmap).mean()
		lrp = calc_lrp([final_boxes], [gt_box])['lrp']
		printer.print_data_row(inds, len(boxes), len(final_boxes), (time.time() - start_time), mae, lrp)
		images_boxes.append(final_boxes)
		gt_boxes.append(gt_box)
		img_info.append([boxmap.shape[0],boxmap.shape[1],inds])
	print()
	print('Total images: {:d}'.format(len(boxmap_dirs)))
	print('Total time: {:0.6f} seconds'.format(time.time() - begin_time))
	print()

	print('# Optimal localization recall precision')
	best_lrp, best_s = calc_optimal_lrp(images_boxes, gt_boxes)
	print(f'Optimal confidence threshold = {best_s:.2f}')
	print(f'oLRP     = {best_lrp["lrp"]:.6f}')
	print(f'oLRP_IoU = {best_lrp["lrp_iou"]:.6f}')
	print(f'oLRP_FP  = {best_lrp["lrp_fp"]:.6f}')
	print(f'oLRP_FN  = {best_lrp["lrp_fn"]:.6f}')
	print()

	print('# Localization recall precision (no confidence threshold)')
	lrp = calc_lrp(images_boxes, gt_boxes, confidence_threshold=None)
	print(f'LRP     = {lrp["lrp"]:.6f}')
	print(f'LRP_IoU = {lrp["lrp_iou"]:.6f}')
	print(f'LRP_FP  = {lrp["lrp_fp"]:.6f}')
	print(f'LRP_FN  = {lrp["lrp_fn"]:.6f}')
	print()

	print('# Average precision')
	for iou_threshold in [0.05, 0.50, 0.70, 0.95]:
		# NOTE: Previous versions of this code were equivalent to using policy='pascal_voc', which is a worse way of
		#       matching boxes.
		ap = calc_ap(images_boxes, gt_boxes, iou_threshold=iou_threshold, policy='coco')
		print(f'AP_AUC@{iou_threshold:.2f} = {ap:.6f}')
	print()

	if args.savefig=='Yes':
		print('Saving figures...')
		results_path = Path('results', args.opt_algorithm, args.boxmap_type)
		results_path.mkdir(exist_ok=True, parents=True)
		for inds, (a_dir, boxes) in enumerate(zip(boxmap_dirs, images_boxes)):
			boxmap = np.load(os.path.join(args.data_path, args.boxmap_type, a_dir))
			fig = plot_boxmap(boxmap, boxes)
			fig.savefig(results_path.joinpath(str(inds) + ".jpg"))
def show_single_map(boxmap_shape,boxes):
	from PIL import Image,ImageDraw
	import matplotlib.pyplot as plt
	boxmap = Image.new(mode = "RGB", size = (boxmap_shape[1],boxmap_shape[0])) 
	draw_dets = ImageDraw.Draw(boxmap)
	for a_box in boxes:
		draw_dets.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="red",fill=None)
	plt.imshow(boxmap)
	plt.show()

if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--data_path', default='./crowdhuman_dataset',
		help='the path to the dataset')
	parser.add_argument('--boxmap_type', default='gt_boxmap', choices=['gt_boxmap', 'pred_boxmap'],
		help='the type of boxmap to use')
	parser.add_argument('--opt_algorithm', default='multipath-pixel-voting', choices=ALGORITHMS.keys(),
		help='the optimization algorithm to use')
	parser.add_argument('--savefig', default='Yes', choices=['Yes', 'No'],
		help='whether to save figures illustrating the results')
	parser.add_argument('--with_clustering', default='No', choices=['Yes', 'No'],
		help='whether to use the box clustering divide-and-conquer trick')
	parser.add_argument('--with_sub_clustering', default='No', choices=['Yes', 'No'],
		help='whether to use the box clustering divide-and-conquer trick')
	parser.add_argument('--with_nms_initialization', default='No', choices=['Yes', 'No'],
		help='whether to use nms outputted best combination of boxmap boxes to start searching')
	parser.add_argument('--low_score_boxes_prune', default='No', choices=['Yes', 'No'],
		help='whether to prune low score boxes')
	parser.add_argument('--with_max_image_size', default=-1, type=int,
		help='whether to downsample large images (use -1 to disable downsampling)')
	parser.add_argument('--with_box_merging',default='No', choices=['Yes','No'],
		help='whether to merge boxes before use to the algorithm')
	parser.add_argument('--merging_iou_thresh',default=.75,
		help='whether to merge boxes before use to the algorithm')
	args = parser.parse_args()
	main(args)
